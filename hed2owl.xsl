<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <xsl:element name="Ontology">

    <xsl:apply-templates select="HED/schema" />

  </xsl:element>
</xsl:template>

<xsl:template match="node" name="node_template">
  
  <xsl:param name="parent" />

  <!-- Exclude the nodes representing values -->
  <xsl:if test="name!='#'">

    <xsl:element name="Declaration">
      <xsl:element name="Class">
        <xsl:attribute name="IRI">
          <xsl:text>#</xsl:text>
          <xsl:value-of select="name"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:element>

    <xsl:if test="$parent">
      <xsl:element name="SubClassOf">

        <!-- Child -->
        <xsl:element name="Class">
          <xsl:attribute name="IRI">
            <xsl:text>#</xsl:text>
            <xsl:value-of select="name"/>
          </xsl:attribute>
        </xsl:element>

        <!-- Parent -->
        <xsl:element name="Class">
          <xsl:attribute name="IRI">
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$parent"/>
          </xsl:attribute>
        </xsl:element>

      </xsl:element>
    </xsl:if>
    
    <!-- Apply the template recursively with the parent name -->
    <xsl:apply-templates select="node">
      <xsl:with-param name="parent"><xsl:value-of select="name"/></xsl:with-param>
    </xsl:apply-templates>
  
  </xsl:if>

</xsl:template>

</xsl:stylesheet>
