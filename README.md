# Convert Hierarchical Event Descriptors into OWL

[Hierarchical Event Descriptors (HED)](https://www.hedtags.org/) are [stored as an XML file](https://github.com/hed-standard/hed-schemas/tree/main/standard_schema/hedxml).

`hed2owl.xsl` file is the XSLT transformation from HED XML to OWL syntax. Currently, it supports only classes and subclasses.

[**Download the OWL file for HED 8.2.0**](https://gitlab.com/api/v4/projects/45068833/jobs/artifacts/main/raw/HED8.2.0.owl?job=generate-owl)

## Dependencies

Requires [`xsltproc` tool](http://xmlsoft.org/xslt/xsltproc.html). You can also use any other processor.

## Execute

Execute the following to transform HED XML into OWL.
```
wget https://raw.githubusercontent.com/hed-standard/hed-schemas/main/standard_schema/hedxml/HED8.1.0.xml
xsltproc hed2owl.xsl HED8.1.0.xml > HED8.1.0.owl
```
